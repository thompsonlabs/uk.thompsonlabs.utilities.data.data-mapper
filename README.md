# README #

### What is DataMapper? ###
DataMapper is a lightweight utility that performs bi-directional mapping operations between standards compliant, JPA Persistence (Domain) Objects 
and any corresponding, developer specified, Data Transfer Objects (DTO). Object graphs of arbitrary depth may be specified and
implicit, cyclic-reference detection and mitigation is also provided.

![Scheme](datamapper-daigram-small.png)

### Getting Setup ###
DataMapper uses the **JitPack** platform as a build and binary distribution agent. What follows are instructions on integrating 
DataMapper into a **Maven** based project. See the [jitpack homepage ](https://www.jitpack.io/) for details on configuring the
utility to work with alternative build tools (i.e Gradle etc). That said, for Maven based projects, the setup process essentially 
involves referecing JitPack and the DataMapper utility in the projects **POM** file:

* **Add reference to JitPack as a Repository:**
```
 <repositories>
        
        <repository>
            <id>jitpack.io</id>
            <url>https://jitpack.io</url>
        </repository>

 </repositories>
```

* **Add DataMapper as a Dependency:**
```
 <dependencies>

        <dependency>
            <groupId>org.bitbucket.thompsonlabs</groupId>
            <artifactId>uk.thompsonlabs.utilities.data.data-mapper</artifactId>
            <version>a09d102688</version>
        </dependency>

  </dependencies>
```

Thats it, simply reload the POM file and rebuild your project as necessary to complete the setup process.


### DataMapper Usage ###

* **Instansiate the DataMapper utility.**

  1.   Grab reference to a DataMapper instance from the factory. The DataMapper is thread-safe and thus a "singleton" instance
  ,as obtained from the factory's **getInstance()** method, should be used where possible.
```Java
DataMapper dataMapper = DataMapperFactory.getInstance();
```

* **Mapping Singular Instances**   

1.   To Map an Entity to a DTO call the appropriate method passing in the entity instance and a reference to the DTO class it 
should be mapped to. A MappingStratergy enum value may be optionally provided to detail how any nested relationships present in 
the Entity object graph should be handled. Where a Mapping Strategy value is omitted, as it is in our example, the default
value: EAGERLY_MAP_SINGLE_RELATIONSHIPS_ONLY is used, which, as its name implies will only map **singular**
(i.e one-to-one,many-to-one and embeddable) relationships:
```Java
      /** Perpartion of a demonstration Entity domain object with nested child Entity (one-to-one relationship)
       * simulates a structure that may be loaded from underlying data store repository in a real application. */
       var bookEntity = new Book(324,"Greatest Book","Giles Thompson","548595");
       var bookAudioEntity = new BookAudio();
       bookAudioEntity.setAudioBookName("Greatest Book Audio");
       bookEntity.setBookAudio(bookAudioEntity);

       /** Issue a call to the DataMapper to map the BookEntity (and all child entities in its object graph) to
        * to a coressponding DTO instance. Here, the resulting DTO instance is then printed to the console. */
       var mappedBookDto = dataMapper.mapToDTO(bookEntity, BookDTO.class);
       System.out.println(mappedBookDto);
```

2.   Conversely, to Map a DTO to an Entity call the appropriate method passing in the DTO instance and a reference to the Entity class it
     should be mapped to:
```Java
     /** Perpartion of a demonstration DTO object with nested child DTO (one-to-one relationship)
       * simulates a structure that may be received into an application from a web service or API. */
       var bookDto = new BookDTO(267236,"Best Book","Giles Thompson","899849");
       var bookAudioDTO = new BookAudioDTO();
       bookAudioDTO.setId("37893949484");
       bookAudioDTO.setAudioBookName("Best Book Audio");
       bookDto.setBookAudio(bookAudioDTO);

       /** Issue a call to the DataMapper to map the BookDTO (and all child DTO's in its object graph) to
         * to a coressponding Entity instance. Here the resulting Entity is then printed to the console. */
        var mappedBookEntity = dataMapper.mapToEntity(bookDto,Book.class);
        System.out.println(mappedBookEntity);
```

* **Mapping Collection Instances**

1.   To Map a Collection of Entities to a collection of DTO's, call the appropriate method passing in the Collection of Entities and a reference
to the DTO class that each entity in the collection should be mapped to; all implementations of Set and List Collections are supported. A MappingStratergy 
enum value may be optionally provided to detail how any nested relationships present in
the Entity object graph should be handled. Where a Mapping Strategy value is omitted, as it is in our example, the default
value: EAGERLY_MAP_SINGLE_RELATIONSHIPS_ONLY is used, which, as its name implies will only map **singular**
(i.e one-to-one,many-to-one and embeddable) relationships in the collection:
```Java
      /** Perpartion of a Collection of demonstration Entity domain objects with nested child Entities (one-to-one relationship)
       *  simulates a structure that may be loaded from underlying data store repository in a real application. */
       var bookEntity1 = new Book(324,"Greatest Book","Giles Thompson","548595");
       var bookAudioEntity1 = new BookAudio();
       bookAudioEntity1.setAudioBookName("Greatest Book Audio");
       bookEntity1.setBookAudio(bookAudioEntity1);

       var bookEntity2 = new Book(423,"Greatest Book 2","Giles Thompson","757857");
       var bookAudioEntity2 = new BookAudio();
       bookAudioEntity2.setAudioBookName("Greatest Book Audio 2");
       bookEntity2.setBookAudio(bookAudioEntity2);

        var bookEntityList = new ArrayList<>();
        bookEntityList.add(bookEntity1);
        bookEntityList.add(bookEntity2);

        /** Issue a call to the DataMapper to map the collection of Entities to a Coressponding Collection of DTO instances. Note that
         *  the collection returned will be symmetric to the collection provided as an argument to the method, i.e if a List of entries
         *  is provided then a List is of entries is returned as a result. All List and Set Collection implementations are supported.
         *  Finally, here the resulting DTO Collection contents is then printed to the console. */
        var mappedBookDtoCollection = dataMapper.mapToDTOCollection(bookEntityList,BookDTO.class);
        mappedBookDtoCollection.stream().map(e->{return e.toString();}).collect(Collectors.toList()).forEach(System.out::println);
```
2.   Conversely, to Map a Collection of DTO's to a collection of Entities, call the appropriate method passing in the Collection of DTO's and a reference
to the Entity class that each DTO in the collection should be mapped to; all implementations of Set and List Collections are supported:
```Java
      /** Perpartion of a Collection of demonstration DTO objects with nested child DTO (one-to-one relationship)
       *  simulates a structure that may be received into an application from a web service or API.  */
       var bookDTO1 = new BookDTO(324,"Greatest Book","Giles Thompson","548595");
       var bookAudioDTO1 = new BookAudioDTO();
       bookAudioDTO1.setId("598689658966");
       bookAudioDTO1.setAudioBookName("Greatest Book Audio");
       bookDTO1.setBookAudio(bookAudioDTO1);

       var bookDTO2 = new BookDTO(423,"Greatest Book 2","Giles Thompson","757857");
       var bookAudioDTO2 = new BookAudioDTO();
       bookAudioDTO2.setId("9896896889698");
       bookAudioDTO2.setAudioBookName("Greatest Book Audio 2");
       bookDTO2.setBookAudio(bookAudioDTO2);

       var bookDtoSet = new HashSet<>();
       bookDtoSet.add(bookDTO1);
       bookDtoSet.add(bookDTO2);

        /** Issue a call to the DataMapper to map the collection of DTO's to a Coressponding Collection of Entity instances. Note that
         *  the collection returned will be symmetric to the collection provided as an argument to the method, i.e if a List of entries
         *  is provided then a List is of entries is returned as a result. All List and Set Collection implementations are supported.
         *  Finally, here the resulting Entity Collection contents is then printed to the console. */
        var mappedBookEntityCollection = dataMapper.mapToEntityCollection(bookDtoSet,Book.class);
        mappedBookEntityCollection.stream().map(e->{return e.toString();}).collect(Collectors.toList()).forEach(System.out::println);
```

### Documentation ###
See the offical [Javadoc](https://thompsonlabs.bitbucket.io/projects/uk-thompsonlabs-utilities-data-data-mapper/javadoc) for full documentation.

### Test Coverage ###
Line Unit Test coverage for the project is currently at circa 85%. You can view the [Full Test Coverage Report](https://thompsonlabs.bitbucket.io/projects/uk-thompsonlabs-utilities-data-data-mapper/test-coverage) for more info. 

### License ###
* MIT

### Contacts ###

* Name: Giles Thompson
* Email: giles@thompsonlabs.uk