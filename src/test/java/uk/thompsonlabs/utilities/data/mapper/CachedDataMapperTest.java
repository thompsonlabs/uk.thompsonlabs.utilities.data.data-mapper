/*
 * Copyright (c) 2021. | Thompsonlabs Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package uk.thompsonlabs.utilities.data.mapper;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;
import uk.thompsonlabs.utilities.data.domain.*;
import uk.thompsonlabs.utilities.data.dtos.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


class CachedDataMapperTest {

    private static DataMapper dataMapper;

    @BeforeAll
    static void beforeAll() {

        dataMapper = DataMapperFactory.getInstance();
    }

    @Test
    void mapToDTO() {

        Book bookEntity = new Book(1,"A Brand new book","Giles Thompson","57848586");

        try {

            var bookDTO = dataMapper.mapToDTO(bookEntity, BookDTO.class, MappingStrategy.EAGERLY_MAP_ALL_RELATIONSHIPS);

            System.out.println(bookDTO);

            Assert.isTrue(bookDTO.getAuthor() == bookEntity.getAuthor(),"Entity Values must be equivalent.");
        }

        catch (DataMappingException e) {
            e.printStackTrace();
        }
    }



    @Test
    void mapToDTO_eagerly_map_all_relationships() {

        Book bookEntity = new Book(1,"A Brand new book","Giles Thompson","57848586");

        BookPublisher bookPublisherEntity1 = new BookPublisher();
        bookPublisherEntity1.setId(1);
        bookPublisherEntity1.setPublisherName("House of Books");

        BookPublisher bookPublisherEntity2 = new BookPublisher();
        bookPublisherEntity2.setId(2);
        bookPublisherEntity2.setPublisherName("Best Stories.");

        //List<BookPublisher> bookPublisherList = new ArrayList<>();
        Set<BookPublisher> bookPublisherList = new HashSet<>();
        bookPublisherList.add(bookPublisherEntity1);
        bookPublisherList.add(bookPublisherEntity2);

        bookEntity.setPublishers(bookPublisherList);

        try {

            var bookDTO = dataMapper.mapToDTO(bookEntity, BookDTO.class,MappingStrategy.EAGERLY_MAP_ALL_RELATIONSHIPS);

            System.out.println(bookDTO);

            Assert.isTrue(bookDTO.getPublishers().size() == bookEntity.getPublishers().size(),"Size of eargely loaded collection should be equivalent.");
        }
        catch (DataMappingException e) {
            e.printStackTrace();
        }


    }

    @Test
    void mapToDTO_eagerly_map_singular_relationships() {

        Book bookEntity = new Book(1,"A Brand new book","Giles Thompson","57848586");

        BookAudio bookAudio = new BookAudio();
        bookAudio.setId("3335353");
        bookAudio.setAudioBookName("Great Audio Version");

        bookEntity.setBookAudio(bookAudio);

        try {

            var bookDTO = dataMapper.mapToDTO(bookEntity, BookDTO.class,MappingStrategy.EAGERLY_MAP_SINGLE_RELATIONSHIPS_ONLY);

            System.out.println(bookDTO);

            Assert.isTrue(bookDTO.getBookAudio().getAudioBookName().equals(bookEntity.getBookAudio().getAudioBookName()),"Book Audio names must match!");
        }
        catch (DataMappingException e) {
            e.printStackTrace();
        }
    }



    @Test
    void mapToDTO_eagerly_map_all_relationships_bi_directional() {

        Book bookEntity = new Book(1,"A Brand new book","Giles Thompson","57848586");

        BookCategory bookCategory = new BookCategory();

        bookCategory.setCategoryName("Fiction");

        Set<BookCategory> booksCategorySet = new HashSet<>();

        Set<Book> booksSet = new HashSet<>();

        booksCategorySet.add(bookCategory);

        booksSet.add(bookEntity);

        bookEntity.setBookCategories(booksCategorySet);

        bookCategory.setBooksInCategory(booksSet);

        try {

            var bookDTO = dataMapper.mapToDTO(bookEntity, BookDTO.class,MappingStrategy.EAGERLY_MAP_ALL_RELATIONSHIPS);

            System.out.println(bookDTO);

            System.out.println(bookDTO.getBookCategories().stream().findFirst());


            Assert.isTrue(bookDTO.getBookCategories().size() == bookEntity.getBookCategories().size(),"Internal Collections must be the same size.");
        }

        catch (DataMappingException e) {
            e.printStackTrace();
        }


    }

    @Test
    void mapToDTO_disable_relationship_mapping(){

        Book bookEntity = new Book(1,"A Brand new book","Giles Thompson","57848586");

        BookCategory bookCategory = new BookCategory();

        bookCategory.setCategoryName("Fiction");

        Set<BookCategory> booksCategorySet = new HashSet<>();

        Set<Book> booksSet = new HashSet<>();

        booksCategorySet.add(bookCategory);

        booksSet.add(bookEntity);

        bookEntity.setBookCategories(booksCategorySet);

        bookCategory.setBooksInCategory(booksSet);

        BookAudio bookAudio = new BookAudio();
        bookAudio.setId("3335353");
        bookAudio.setAudioBookName("Great Audio Version");

        bookEntity.setBookAudio(bookAudio);

        try {

            var bookDTO = dataMapper.mapToDTO(bookEntity, BookDTO.class,MappingStrategy.DISABLE_RELATIONSHIP_MAPPING);

            System.out.println(bookDTO);

            Assert.isTrue(bookDTO.getBookCategories() == null && bookDTO.getBookAudio() == null,"Relationship Mappings are disabled, All references to associated entities should be null.");
        }

        catch (DataMappingException e) {
            e.printStackTrace();
        }
    }

    @Test
    void mapToEntity() {

        BookDTO bookDTO = new BookDTO(1,"A Brand new book","Giles Thompson","57848586");

        try {

            var bookEntity = dataMapper.mapToEntity(bookDTO,Book.class);

            System.out.println(bookEntity);

            Assert.isTrue(bookEntity.getAuthor().equals(bookDTO.getAuthor()),"Mapped Attributes must be consistent.");
        }

        catch (DataMappingException e) {
            e.printStackTrace();
        }
    }

    @Test
    void mapToEntity_with_mapped_super_class() {

        BookAudioDTO bookAudioDTO = new BookAudioDTO();
        bookAudioDTO.setId("94595894589458");
        bookAudioDTO.setAudioBookName("The best book audio.");
        bookAudioDTO.setCreatedAt(System.currentTimeMillis());

        try {

            var bookAudioEntity = dataMapper.mapToEntity(bookAudioDTO,BookAudio.class);

            System.out.println(bookAudioEntity);

            Assert.isTrue(bookAudioEntity.getId().equals(bookAudioDTO.getId()),"Id values must match.");
        }
        catch (DataMappingException e) {
            e.printStackTrace();
        }
    }

    @Test
    void mapToEntity_eagerly_map_singular_relationships() {

        BookDTO bookDTO = new BookDTO(1,"A Brand new book","Giles Thompson","57848586");

        BookAudioDTO bookAudioDTO = new BookAudioDTO();
        bookAudioDTO.setId("94595894589458");
        bookAudioDTO.setAudioBookName("The best book audio.");
        bookAudioDTO.setCreatedAt(System.currentTimeMillis());

        bookDTO.setBookAudio(bookAudioDTO);

        try {

            var bookEntity = dataMapper.mapToEntity(bookDTO,Book.class);

            System.out.println(bookEntity);

            Assert.isTrue(bookEntity.getBookAudio().getCreatedAt() == bookDTO.getBookAudio().getCreatedAt(),"Mapped Relationship attributes must be consistent.");
        }

        catch (DataMappingException e) {
            e.printStackTrace();
        }
    }



    @Test
    void mapToEntity_eagerly_map_all_relationships() {

        BookDTO bookDTO = new BookDTO(1,"A Brand new book","Giles Thompson","57848586");

        BookPublisherDTO bookPublisherDTO1 = new BookPublisherDTO();
        bookPublisherDTO1.setId(1);
        bookPublisherDTO1.setPublisherName("Stories of London");

        BookPublisherDTO bookPublisherDTO2 = new BookPublisherDTO();
        bookPublisherDTO2.setId(2);
        bookPublisherDTO2.setPublisherName("Braveweights");

        Set<BookPublisherDTO> bookPublisherDTOSet = new HashSet<>();
        bookPublisherDTOSet.add(bookPublisherDTO1);
        bookPublisherDTOSet.add(bookPublisherDTO2);

        bookDTO.setPublishers(bookPublisherDTOSet);

        try {

            var bookEntity = dataMapper.mapToEntity(bookDTO,Book.class);

            System.out.println(bookEntity);

            Assert.isTrue(bookEntity.getPublishers().size() == bookDTO.getPublishers().size(),"Mapped collections must be the same length.");
        }

        catch (DataMappingException e) {
            e.printStackTrace();
        }

    }

    @Test
    void mapToEntity_eagerly_map_all_relationships_bi_directional() {

        BookDTO bookDTO = new BookDTO(1,"A Brand new book","Giles Thompson","57848586");

        BookCategoryDTO bookCategoryDTO = new BookCategoryDTO();
        bookCategoryDTO.setId("2233");
        bookCategoryDTO.setCategoryName("Current Affairs");

        Set<BookDTO> bookDTOSet = new HashSet<>();

        Set<BookCategoryDTO> bookCategoryDTOSet = new HashSet<>();

        bookDTOSet.add(bookDTO);

        bookCategoryDTOSet.add(bookCategoryDTO);

        bookDTO.setBookCategories(bookCategoryDTOSet);

        bookCategoryDTO.setBooksInCategory(bookDTOSet);

        try {

            var bookEntity = dataMapper.mapToEntity(bookDTO,Book.class);

            System.out.println(bookEntity);

            System.out.println(bookEntity.getBookCategories().stream().findFirst());

            Assert.isTrue(bookEntity.getBookCategories().size() == bookDTO.getBookCategories().size(),"Mapped collections must be the same length.");
        }

        catch (DataMappingException e) {
            e.printStackTrace();
        }

    }

    @Test
    void mapToDTOList() {

        Book bookEntity1 = new Book(1,"A Brand new book 1","Giles Thompson","57848586");
        Book bookEntity2 = new Book(2,"A Brand new book 2","Giles Thompson","67658768");
        Book bookEntity3 = new Book(3,"A Brand new book 3","Giles Thompson","98689586");

        var bookEntityList = List.of(bookEntity1,bookEntity2,bookEntity3);

        try {

            var bookDTOList = dataMapper.mapToDTOCollection(bookEntityList,BookDTO.class,MappingStrategy.EAGERLY_MAP_ALL_RELATIONSHIPS);

            //System.out.println(bookDTOList.stream().map(e->{ return e.toString(); }).toList());

            Assert.isTrue(bookDTOList.size() == bookEntityList.size(),"Mapped collection must be of the same length.");
        }
        catch (DataMappingException e) {
            e.printStackTrace();
        }

    }

    @Test
    void mapToEntityList() {

        BookDTO bookDTO1 = new BookDTO(1,"A Brand new book 1","Giles Thompson","57848586");
        BookDTO bookDTO2 = new BookDTO(2,"A Brand new book 2","Giles Thompson","67658768");
        BookDTO bookDTO3 = new BookDTO(3,"A Brand new book 3","Giles Thompson","98689586");

        var bookDTOList = List.of(bookDTO1,bookDTO2,bookDTO3);

        try {

            var bookEntityList = dataMapper.mapToEntityCollection(bookDTOList,Book.class);

            //System.out.println(bookEntityList.stream().map(e->{return e.toString();}).toList());

            Assert.isTrue(bookEntityList.size() == bookDTOList.size(),"Mapped collection must be of the same length");
        }
        catch (DataMappingException e) {
            e.printStackTrace();
        }
    }

    @Test
    void mapToDTO_eagerly_map_singular_relationships_with_default_stratergy(){;

        Book bookEntity = new Book(1,"A Brand new book","Giles Thompson","57848586");

        BookAudio bookAudio = new BookAudio();
        bookAudio.setId("3335353");
        bookAudio.setAudioBookName("Great Audio Version");

        bookEntity.setBookAudio(bookAudio);

        try {

            //nb: here, for the purposes of this test, we DO NOT explictly specify a MappingStatergy which should result in the
            //    default "EAGERLY_MAP_SINGLE_RELATIONSHIPS_ONLY" value being used, thereby yeilding the same net effect (i.e the
            //    test should still pass.)
            var bookDTO = dataMapper.mapToDTO(bookEntity, BookDTO.class);

            System.out.println(bookDTO);

            Assert.isTrue(bookDTO.getBookAudio().getAudioBookName().equals(bookEntity.getBookAudio().getAudioBookName()),"Book Audio names must match!");
        }
        catch (DataMappingException e) {
            e.printStackTrace();
        }

    }

    @Test
    void mapToEntity_check_entity_generated_id_is_preserved_where_dto_id_is_null(){

        //we purposely omit setting the id of the DTO this should cause the
        //entity super class (i.e AbstractBaseEntity) custom generated id to be used.
        BookCategoryDTO bookCategoryDTO = new BookCategoryDTO();
        bookCategoryDTO.setCategoryName("The best category.");

        try {

            var bookCategoryEntity = dataMapper.mapToEntity(bookCategoryDTO,BookCategory.class);

            System.out.println(bookCategoryEntity);

            Assert.notNull(bookCategoryEntity.getId(),"Entity id should not be set to null when DTO has a null id. Its internally generated value should be used instead.");
        }

        catch (DataMappingException e) {
            e.printStackTrace();
        }
    }

}