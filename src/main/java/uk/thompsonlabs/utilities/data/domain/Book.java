/*
 * Copyright (c) 2021. | Thompsonlabs Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package uk.thompsonlabs.utilities.data.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Book {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;




    @OneToMany
    //private List<BookPublisher> publishers;
    private Set<BookPublisher> publishers;

    @OneToOne
    private BookAudio bookAudio;

    @ManyToMany
    @JoinTable(
            name = "book_to_book_category",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "book_cateogry_id"))
    private Set<BookCategory> bookCategories;

    private String title;

    private String author;

    private String ISBN;

    public Book(){

    }

    public Book(Integer id, String title, String author, String ISBN) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    /**
    public List<BookPublisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<BookPublisher> publishers) {
        this.publishers = publishers;
    }
     */

    public Set<BookPublisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(Set<BookPublisher> publishers) {
        this.publishers = publishers;
    }

    public BookAudio getBookAudio() {
        return bookAudio;
    }

    public void setBookAudio(BookAudio bookAudio) {
        this.bookAudio = bookAudio;
    }

    public Set<BookCategory> getBookCategories() {
        return bookCategories;
    }

    public void setBookCategories(Set<BookCategory> bookCategories) {
        this.bookCategories = bookCategories;
    }

    public String toString(){

        var bookAudioData = "";
        if(bookAudio != null){

            bookAudioData = bookAudioData.concat("{id: "+bookAudio.getId()+", bookAudioName: "+bookAudio.getAudioBookName()+", craetedAt: "+bookAudio.getCreatedAt()+"}");
        }

        var bookPublisherData = "";
        if(publishers != null){

            StringBuilder s = new StringBuilder();

            for(var publisher : publishers ){

                s.append("{ id:")
                        .append(publisher.getId())
                        .append(", publisherName: ")
                        .append(publisher.getPublisherName())
                        .append("}");
            }
            bookPublisherData = s.toString();
        }

        var bookCategoryData = "";
        if(bookCategories != null){

            StringBuilder s = new StringBuilder();

            for(var bookCategory : bookCategories ){

                s.append("{ id:")
                        .append(bookCategory.getId())
                        .append(", categoryName: ")
                        .append(bookCategory.getCategoryName())
                        .append("}");
            }

            bookCategoryData = s.toString();
        }

        return "{id: "+id+", title: "+title+", author: "+author+", ISBN: "+ISBN+", bookAudio: "+bookAudioData+", publishers: "+bookPublisherData+", categories: "+bookCategoryData+"}";
    }
}


