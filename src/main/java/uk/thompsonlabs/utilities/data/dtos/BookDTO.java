/*
 * Copyright (c) 2021. | Thompsonlabs Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package uk.thompsonlabs.utilities.data.dtos;

import uk.thompsonlabs.utilities.data.domain.Book;

import java.util.Set;

public class BookDTO {

    private Integer id;
    private String title;
    private String author;
    private String ISBN;



    private Set<BookPublisherDTO> publishers;

    private BookAudioDTO bookAudio;

    private Set<BookCategoryDTO> bookCategories;

    public static BookDTO fromEntity(Book aBookEntity){

        return new BookDTO(aBookEntity.getId(),
                           aBookEntity.getTitle(),
                           aBookEntity.getAuthor(),
                           aBookEntity.getISBN());
    }

    public BookDTO(Integer id, String title, String author, String ISBN) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
    }

    public BookDTO(){

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    /**
    public List<BookPublisherDTO> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<BookPublisherDTO> publishers) {
        this.publishers = publishers;
    }
     */

    public Set<BookPublisherDTO> getPublishers() {
        return publishers;
    }

    public void setPublishers(Set<BookPublisherDTO> publishers) {
        this.publishers = publishers;
    }

    public String toString(){

        var publisherData = "";
        if(publishers != null && !publishers.isEmpty()){

            StringBuilder sb = new StringBuilder();

            for(var curPub : publishers){

                sb.append("{").append("id: ").append(curPub.getId()).append(", name: ").append(curPub.getPublisherName()).append("}\n");

            }

            publisherData = sb.toString();
        }

        var bookAudioData = "";
        if(bookAudio != null){

            bookAudioData = bookAudioData.concat("{ id: ").concat(bookAudio.getId()).concat(", bookAudioName: ").concat(bookAudio.getAudioBookName()).concat(", createdAt: ").concat(Long.toString(bookAudio.getCreatedAt())).concat(" }");
        }

        var bookCategoryData = "";
        if(bookCategories != null ){

            StringBuilder sb = new StringBuilder();

            for(var curBookCat : bookCategories){

                sb.append("{ id: ").append(curBookCat.getId()).append(", categoryName: ").append(curBookCat.getCategoryName()).append(" }");
            }

            bookCategoryData = sb.toString();
        }

        return "BookDTO{ id: "+id+", title: "+title+", author: "+author+", ISBN: "+ISBN+", publishers: "+publisherData+", bookAudio: "+bookAudioData+" bookCategories: "+bookCategoryData+"}";
    }

    public BookAudioDTO getBookAudio() {
        return bookAudio;
    }

    public void setBookAudio(BookAudioDTO bookAudio) {
        this.bookAudio = bookAudio;
    }

    public Set<BookCategoryDTO> getBookCategories() {
        return bookCategories;
    }

    public void setBookCategories(Set<BookCategoryDTO> bookCategories) {
        this.bookCategories = bookCategories;
    }
}


