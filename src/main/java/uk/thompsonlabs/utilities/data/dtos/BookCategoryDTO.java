/*
 * Copyright (c) 2021. | Thompsonlabs Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package uk.thompsonlabs.utilities.data.dtos;



import java.util.Set;

public class BookCategoryDTO {

    private String id;

    private String categoryName;

    private Set<BookDTO> booksInCategory;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Set<BookDTO> getBooksInCategory() {
        return booksInCategory;
    }

    public void setBooksInCategory(Set<BookDTO> booksInCategory) {
        this.booksInCategory = booksInCategory;
    }

    public String toString(){

        var booksData = "";

        if(booksInCategory != null){

            StringBuilder sb = new StringBuilder();

            for(var book : booksInCategory){

                sb.append("{ id: ")
                        .append(book.getId())
                        .append(", title: ")
                        .append(book.getTitle())
                        .append(", author: ")
                        .append(book.getAuthor())
                        .append(", ISBN: ")
                        .append(book.getISBN())
                        .append(" }");
            }

            booksData = sb.toString();
        }

        return "BookCategoryDTO{ id: "+id+", categoryName: "+categoryName+", booksInCategory: "+booksData+" }";
    }
}
