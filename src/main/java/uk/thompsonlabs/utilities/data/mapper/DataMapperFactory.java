/*
 * Copyright (c) 2021. | Thompsonlabs Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package uk.thompsonlabs.utilities.data.mapper;

/** Factory, returns reference to DataMapper implementations. */
public class DataMapperFactory {

    private static DataMapper dataMapperInstance;

    /**
     * Creates and returns a new DataMapper Implementation instance.
     * @return DataMaper A new DataMapper implmentation instance.
     **/
    public static DataMapper newInstance(){

        return new CachedDataMapper();
    }

    /**
     * Returns a singleton instance to a DataMapper implementation instance.
     * The initial call to this method will necessarily create the instance, any and
     * all subsequent calls to this method will then return the SAME instance.
     * @return DataMapper A singular reference to a DataMapper instance.
     **/
    public static DataMapper getInstance(){

        if(dataMapperInstance == null){

            dataMapperInstance = new CachedDataMapper();
        }

        return dataMapperInstance;
    }
}
