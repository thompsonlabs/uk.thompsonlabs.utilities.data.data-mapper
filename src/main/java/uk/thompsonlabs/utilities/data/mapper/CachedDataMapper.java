/*
 * Copyright (c) 2021. | Thompsonlabs Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package uk.thompsonlabs.utilities.data.mapper;

import org.springframework.beans.BeanUtils;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An implementation of a DataMapper that utilises an internal cache to optimize the performance
 * of the various dynamic mapping operations.
 * @author Giles Thompson
 * @version 1.0
 */
public final class CachedDataMapper implements DataMapper {

    private final ClassMetaDataCache classMetaDataCache = new ClassMetaDataCache();

    @Override
    public <E, D> D mapToDTO(E entityInstance, Class<? extends D> DTOClass, MappingStrategy... mappingStrategy) throws DataMappingException {

        var mappingStrat = (mappingStrategy == null || mappingStrategy.length == 0) ? null : mappingStrategy[0];

        return mapToDTO(entityInstance,DTOClass,mappingStrat,null);
    }

    @Override
    public <D, E> E mapToEntity(D DTOInstance, Class<? extends E> entityClass) throws DataMappingException {

        return mapToEntity(DTOInstance,entityClass,null);
    }

    @Override
    public <E, D> Collection<D> mapToDTOCollection(Collection<? extends E> entityInstanceCollection, Class<? extends D> DTOClass, MappingStrategy... mappingStrategy) throws DataMappingException {

        var mappingStrat = (mappingStrategy == null || mappingStrategy.length == 0) ? null : mappingStrategy[0];

        return mapToDTOCollection(entityInstanceCollection,DTOClass,mappingStrat,null);
    }

    @Override
    public <D, E> Collection<E> mapToEntityCollection(Collection<? extends D> DTOInstanceCollection, Class<? extends E> entityClass) throws DataMappingException {

        return mapToEntityCollection(DTOInstanceCollection,entityClass,null);
    }



                                                           //HELPER METHODS



    private <D, E> E mapToEntity(D DTOInstance, Class<? extends E> entityClass,Map<Integer,Object> processedInstances) throws DataMappingException {

        if(DTOInstance == null)
            throw new DataMappingException("A non-null DTO instance must be provided.");

        if(entityClass == null)
            throw new DataMappingException("A non-null entity reference must be specified.");

        if(processedInstances == null)
            processedInstances = new HashMap<>();

        //curically if the DTO instance has ALREADY been (or in the process of being) mapped, simply return that (DTO) instance.
        if(processedInstances.containsKey(DTOInstance.hashCode()))
            return (E) processedInstances.get(DTOInstance.hashCode());


        try {

            //get entity classs meta data
            var DTOClassMetaData = this.loadOrGenerateClassMetaData(DTOInstance.getClass(),false);

            //get DTO class meta data
            var entityClassMetaData = this.loadOrGenerateClassMetaData(entityClass,true);

            //create new DTO instance
            var entityInstance = entityClassMetaData.getClassConsturctor().newInstance();

            //store the Entity instance (along with associated DTO) to our map to mark it as "being processed" thereby preventing cyclic processing
            processedInstances.put(DTOInstance.hashCode(),entityInstance);

            //grab reference to all entity fields.
            Field[] entityFields = entityClassMetaData.getFields();

            //populate entity fields from the value assigned to to the corresponding DTO.
            for(Field entityField : entityFields ){

                Field targetFieldOnDTO = DTOClassMetaData.getFieldByName(entityField.getName());

                if(targetFieldOnDTO != null){

                    //grab the current runtime value of the target field on the provided DTO instance
                    var targetFieldOnDTOValue = targetFieldOnDTO.get(DTOInstance);

                    //providing the value of the field on the DTO instance is not null we beging processing...
                    if(targetFieldOnDTOValue != null) {

                        if (BeanUtils.isSimpleValueType(entityField.getType())) {

                            entityField.set(entityInstance, targetFieldOnDTO.get(DTOInstance));

                        }
                        else {

                            //Note: Were we have a COMPLEX field type....
                            //if its a collection then we call the collection public interface method of this class to handle processing of all the elements.
                            if (Collection.class.isAssignableFrom(entityField.getType())) {

                                    //get reference to the DTO instance collection (that is required to be converted to a Entity collection)
                                    var DTOInstanceCol = (Collection) targetFieldOnDTOValue;

                                    //Get intented type of the collection class on the DTO
                                    Class collectionType = (Class) ((ParameterizedType) entityField.getGenericType()).getActualTypeArguments()[0];

                                    //call into our public interface method to convert the collection of DAP to entities
                                    var entityCollection = this.mapToEntityCollection(DTOInstanceCol, collectionType,processedInstances);

                                    //finally set the field on our top-level entity instance.
                                    entityField.set(entityInstance, entityCollection);

                            }

                            //Otherwise if its singular (i.e NOT a collection) then we call this method recusively to
                            //process the object and any children
                            else {

                                    //get referecne to the entity instance.
                                    var childDTOInstance = targetFieldOnDTOValue;

                                    //entity type
                                    var childEntityType = entityField.getType();

                                    //call ourselves recusrively to process the child entity (i.e convert it to a DTO)
                                    var childEntity = mapToEntity(childDTOInstance, childEntityType,processedInstances);

                                    //finally set the field on our top-level entity instance
                                    entityField.set(entityInstance, childEntity);


                            }

                        }
                    }
                }

            }

            //finally where the entity has a matched superclass populate any matching fields on that class.
            Class entityMappedSuperClass;
            if((entityMappedSuperClass = entityClassMetaData.getMappedSuperClass()) != null){

                var entityMappedSuperClassMetaData = this.loadOrGenerateClassMetaData(entityMappedSuperClass,false);

                for(Field entitySuperClassField : entityMappedSuperClassMetaData.getFields() ){

                    Field targetFieldOnDTOForMappedSuperClass = DTOClassMetaData.getFieldByName(entitySuperClassField.getName());

                    if(targetFieldOnDTOForMappedSuperClass != null && targetFieldOnDTOForMappedSuperClass.get(DTOInstance) != null){

                        targetFieldOnDTOForMappedSuperClass.setAccessible(true);

                        entitySuperClassField.set(entityInstance,targetFieldOnDTOForMappedSuperClass.get(DTOInstance));
                    }
                }

            }

            return (E) entityInstance;
        }
        catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {

            throw new DataMappingException("An exception occurred whilst map DTO to entity",e);
        }
    }

    private <D, E> Collection<E> mapToEntityCollection(Collection<? extends D> DTOInstanceCollection, Class<? extends E> entityClass,Map<Integer,Object> processedInstances) throws DataMappingException {

        if(DTOInstanceCollection == null || DTOInstanceCollection.isEmpty())
            return null;

        if(entityClass == null)
            throw new DataMappingException("A non-null entity class reference must be provided.");

        if(processedInstances == null)
            processedInstances = new HashMap<>();

        final Collection entityItemCollection = (List.class.isAssignableFrom(DTOInstanceCollection.getClass())) ? new ArrayList<>() : new HashSet<>();

        for(var DTOCollectionItem : DTOInstanceCollection){

            entityItemCollection.add(this.mapToEntity(DTOCollectionItem,entityClass,processedInstances));
        }
        return entityItemCollection;
    }

    private <E, D> Collection<D> mapToDTOCollection(Collection<? extends E> entityInstanceCollection, Class<? extends D> DTOClass, MappingStrategy mappingStrategy,Map<Integer,Object> processedInstances) throws DataMappingException {


        if(entityInstanceCollection == null || entityInstanceCollection.isEmpty())
            return null;

        if(DTOClass == null)
            throw new DataMappingException("A non-null DTO Class reference must be provided.");

        if(processedInstances == null)
            processedInstances = new HashMap<>();

        if(mappingStrategy == null)
            mappingStrategy = MappingStrategy.EAGERLY_MAP_SINGLE_RELATIONSHIPS_ONLY;

        final Collection DTOItemCollection = (List.class.isAssignableFrom(entityInstanceCollection.getClass())) ? new ArrayList<>() : new HashSet<>();

        for( var curEntityListItem : entityInstanceCollection){

            DTOItemCollection.add(this.mapToDTO(curEntityListItem,DTOClass,mappingStrategy,processedInstances));
        }

        return DTOItemCollection;
    }

    private <E, D> D mapToDTO(E entityInstance, Class<? extends D> DTOClass, MappingStrategy mappingStrategy,Map<Integer,Object> processedInstances) throws DataMappingException{

        if(entityInstance == null)
            throw new DataMappingException("A non-null entity instance must be specified.");

        if(DTOClass == null)
            throw new DataMappingException("A non-null DTO Class reference must be provided.");

        if(processedInstances == null)
            processedInstances = new HashMap<>();

        if(mappingStrategy == null)
            mappingStrategy = MappingStrategy.EAGERLY_MAP_SINGLE_RELATIONSHIPS_ONLY;


        //curically if the entitiy instance has ALREADY been (or in the process of being) mapped, simply return that (DTO) instance.
        if(processedInstances.containsKey(entityInstance.hashCode()))
            return (D) processedInstances.get(entityInstance.hashCode());


        try {

            //get entity classs meta data
            var entityClassMetaData = this.loadOrGenerateClassMetaData(entityInstance.getClass(),true);

            //get DTO class meta data
            var DTOClassMetaData = this.loadOrGenerateClassMetaData(DTOClass,false);

            //create new DTO instance
            var DTOInstance = DTOClassMetaData.getClassConsturctor().newInstance();

            //store the DTO instance (along with associated entity) to our map to mark it as "being processed" thereby preventing cyclic processing
            processedInstances.put(entityInstance.hashCode(),DTOInstance);

            //grab reference to all DTO fields.
            Field[] DTOFields = DTOClassMetaData.getFields();

            //populate DTO fields from the value assigned to to the corresponding entity.
            for(Field curDTOField : DTOFields ){

                Field targetFieldOnEntity = entityClassMetaData.getFieldByName(curDTOField.getName());

                if(targetFieldOnEntity != null){

                    //grab the current runtime value of the target field on the provided entity instance
                    var targetFieldOnEntityValue = targetFieldOnEntity.get(entityInstance);

                    //providing the value of the field on the entitiy instance is not null we beging processing...
                    if(targetFieldOnEntityValue != null) {

                        if (BeanUtils.isSimpleValueType(curDTOField.getType())) {

                            curDTOField.set(DTOInstance, targetFieldOnEntity.get(entityInstance));

                        }
                        else {

                            //Note: Were we have a COMPLEX field type....
                            //if its a collection then we call the collection public interface method of this class to handle processing of all the elements.
                            if (Collection.class.isAssignableFrom(curDTOField.getType())) {

                                if(mappingStrategy == MappingStrategy.EAGERLY_MAP_ALL_RELATIONSHIPS) {

                                    //get reference to the entitiy instance collection (that is required to be converted to a DTO collection)
                                    var entityInstanceCol = (Collection<E>) targetFieldOnEntityValue;

                                    //Get intented type of the collection class on the DTO
                                    Class collectionType = (Class) ((ParameterizedType) curDTOField.getGenericType()).getActualTypeArguments()[0];

                                    //call into our public interface method to convert the collection of entities to DTO
                                    var DTOCollection = this.mapToDTOCollection(entityInstanceCol, collectionType, mappingStrategy, processedInstances);

                                    //finally set the field on our top-level DTO instance.
                                    curDTOField.set(DTOInstance, DTOCollection);

                                }

                            }

                            //Otherwise if its singular (i.e NOT a collection) then we call this method recusively to
                            //process the object and any children
                            else {

                                if(mappingStrategy == MappingStrategy.EAGERLY_MAP_SINGLE_RELATIONSHIPS_ONLY || mappingStrategy == MappingStrategy.EAGERLY_MAP_ALL_RELATIONSHIPS) {

                                    //get referecne to the entity instance.
                                    var childEntityInstance = targetFieldOnEntityValue;

                                    //DTO type
                                    var childDTOType = curDTOField.getType();

                                    //call ourselves recusrively to process the child entity (i.e convert it to a DTO)
                                    var childDTO = mapToDTO(childEntityInstance, childDTOType, mappingStrategy, processedInstances);

                                    //finally set the field on our top-level DTO instance
                                    curDTOField.set(DTOInstance, childDTO);

                                }
                            }

                        }
                    }
                }

            }

            //finally where the entity has a matched superclass populate any matching fields on that class.
            Class entityMappedSuperClass;
            if((entityMappedSuperClass = entityClassMetaData.getMappedSuperClass()) != null){

                var entityMappedSuperClassMetaData = this.loadOrGenerateClassMetaData(entityMappedSuperClass,false);

                for(Field curDTOField : DTOFields ){

                    Field targetFieldOnEntityMappedSuperClass = entityMappedSuperClassMetaData.getFieldByName(curDTOField.getName());

                    if(targetFieldOnEntityMappedSuperClass != null){

                        curDTOField.set(DTOInstance,targetFieldOnEntityMappedSuperClass.get(entityInstance));
                    }
                }

            }

            return (D) DTOInstance;
        }
        catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {

            throw new DataMappingException("An exception occurred whilst map Entity to DTO",e);
        }

    }



    /**
     * Loads ClassMetaData from the cache and returns it where available, where the metadata is not in the
     * cache it is firstly generated (via the undertaking of Reflective/Introspective operations), stored/back-filled
     * to the cache, for subsequent use as necessary, before being finally returned to the caller.
     * @param targetClass The class to locate ClassMetaData for.
     * @param isEntity A boolean flag which specifies whether or not the provided class is an Entity
     *                 where this IS the case then extra steps will be taken to resolve a mapped
     *                 super class where one exists.
     * @return
     * @throws NoSuchMethodException
     */
    private ClassMetaData loadOrGenerateClassMetaData(Class targetClass,boolean isEntity) throws NoSuchMethodException, DataMappingException {

        /**
             We access the cache from within the context of a Synchronized Block since
            a SINGLE instance of this CacheDataMapper may be used to service requests
            across MANY concurrent Threads.
         */
        ClassMetaData classMetaData = null;

        synchronized (classMetaDataCache) {

            classMetaData = this.classMetaDataCache.loadFromCache(targetClass.getName());
        }

        if(classMetaData == null){

            Logger.getLogger(CachedDataMapper.class.getName()).log(Level.INFO,"Cache-Miss: Generating Class Metadata for class: "+targetClass.getName());

            var constructor = targetClass.getDeclaredConstructor();

            constructor.setAccessible(true);

            var fields = targetClass.getDeclaredFields();

            classMetaData = new ClassMetaData();

            classMetaData.setClassConsturctor(constructor);


            for (Field curField : fields){

               curField.setAccessible(true);

               classMetaData.addClassField(curField);

            }

            //Next we determine if the entity has a SuperClass annotated with @MappedSuperClass.
            //in its heirarchy...
            if(isEntity){

                final Class mappedSuperClass = locateMappedSuperClass(targetClass);

                if(mappedSuperClass != null) {
                    classMetaData.setMappedSuperClass(mappedSuperClass);
                }
            }

            synchronized (classMetaDataCache){

                classMetaDataCache.storeToCache(targetClass.getName(),classMetaData);
           }
        }

         return classMetaData;
    }

    /** Returns the TYPE of a Generic Collection. */
    public static Class getCollectionTypeParameterClass(Class aCollectionClass){
        System.out.println("Collection class is: "+aCollectionClass);
        Type type = aCollectionClass.getGenericSuperclass();
        System.out.println("Generic super class is: "+type);
        ParameterizedType paramType = (ParameterizedType) type;
        return (Class) paramType.getActualTypeArguments()[0];
    }

    /**
     *  We attempt to locate an super class in the target class inheritence heirarchy that is
     *  annotated with the @MappedSuperclass annotation. This class MUST, in turn, contain a
     *  field annotated with @Id. This method will call itself recursively until such time
     *  that a satisfactory class if found our attempts against all super classes in the hierarchy
     *  have been exhausted.
     *  */
    private static Class locateMappedSuperClass(Class targetClass) {

        Class superClass = targetClass.getSuperclass();

        if(superClass == null) {
            return null;
        }
        else{

            if(classHasMappedSuperClassAnnotation(superClass)){

                return superClass;
            }

            else{

                return locateMappedSuperClass(superClass);
            }
        }

    }

    private static boolean classHasMappedSuperClassAnnotation(Class aClass){

        Annotation annotation = aClass.getAnnotation(MappedSuperclass.class);

        if(annotation != null)
            return true;

        return false;
    }
    private static boolean fieldHasIdAnnotation(Field aField){

        Annotation[] annotations = aField.getDeclaredAnnotations();

        for(Annotation curAnno : annotations)
            if(curAnno instanceof Id)
                return true;

        return false;
    }





                                                            //HELPER CLASSES

    /**
     * Cache used to hold Class Metadata, obtained from having undertaken relatively expensive reflective and/or introspection
     *  lookups. The goal is to ONLY undetake this process ONCE per unique class and then proceed to cache and return data
     *  from the cache in this respect thereafter.
     *  */
    static final class ClassMetaDataCache{

        private final Map<String,ClassMetaData> classNameToMetaDataMap;

        ClassMetaDataCache() {

            this.classNameToMetaDataMap = new HashMap<>();
        }

        void storeToCache(String className,ClassMetaData classMetaData){

            this.classNameToMetaDataMap.put(className,classMetaData);
        }

        ClassMetaData loadFromCache(String className){

            return this.classNameToMetaDataMap.get(className);
        }
    }

    /**
     *  Used to hold Class Metadata obtain from expensive reflection and introspection calls,
     *  instances of these metadata classes are ultimately cached to negate the need to have
     *  repeatedly undertake operation to obtain this data.
     * */
    static final class ClassMetaData{

        private Constructor classConsturctor;

        private Class mappedSuperClass;

        private final  Map<String,Field> classFieldsMap;

        ClassMetaData() {
            this.classFieldsMap = new HashMap<>();
        }

        Constructor getClassConsturctor() {
            return classConsturctor;
        }

        void setClassConsturctor(Constructor classConsturctor) {
            this.classConsturctor = classConsturctor;
        }

        Map<String, Field> getClassFieldsMap() {
            return classFieldsMap;
        }

        Field[] getFields(){

            return this.classFieldsMap.values().toArray(new Field[0]);
        }

        void addClassField(Field aField){

            this.classFieldsMap.put(aField.getName(),aField);
        }

        Field getFieldByName(String fieldName){

            return this.classFieldsMap.get(fieldName);
        }

        Class getMappedSuperClass() {
            return mappedSuperClass;
        }

        void setMappedSuperClass(Class mappedSuperClass) {
            this.mappedSuperClass = mappedSuperClass;
        }

    }
}
