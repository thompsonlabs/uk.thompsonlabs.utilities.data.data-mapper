/*
 * Copyright (c) 2021. | Thompsonlabs Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package uk.thompsonlabs.utilities.data.mapper;

import java.util.Collection;

/**
 * A lightweight utility that performs bi-directional mapping operations between
 * standards compliant JPA Persistence Objects and any corresponding, arbitrarily
 * specified, Data Transfer Objects (DTO).
 * @author Giles Thompson
 * @version 1.0
 *
 */
public interface DataMapper {


    /**
     * Maps the specified Entity to an equivalent DTO of the specified type; all fields
     * with the same name are automatically mapped.
     * @param entityInstance The Entity instance that should be mapped to a DTO.
     * @param DTOClass DTO Class Reference.
     * @param mappingStrategy The mapping strategy that should be utilised for the operation.
     * @param <E> The Entity Type
     * @param <D> The DTO Type
     * @return D An instance of the resulting (dynamically mapped) DTO
     * @throws DataMappingException Where an error occurs during the mapping process.
     */
    public <E,D> D mapToDTO(E entityInstance, Class<? extends D> DTOClass, MappingStrategy... mappingStrategy) throws DataMappingException;



    /**
     * Maps the specified DTO instance to an equivalent Entity of the specified type; all fields
     * with the same name are automatically mapped.
     * @param DTOInstance The DTO Instance that should be mapped to an Entity.
     * @param entityClass Entity Class Reference.
     * @param <D> The DTO Type
     * @param <E> The Entity Type.
     * @return E An instance of the resulting (dynamically mapped) Entity
     * @throws DataMappingException Where an error occurs during the mapping process.
     */
    public <D,E> E mapToEntity(D DTOInstance,Class<? extends E> entityClass) throws DataMappingException;



    /**
     * Maps the specified Collection of Entities to an equivalent Collection of DTOs of the specified type; all fields
     * with the same name are automatically mapped. Note that the returned collection type is symmetric to the collection
     * provided to the method as input. That is, if a List of Entities is provided the a List of DTOs will be returned.
     * All List and Set implementations are supported.
     * @param <E> The Entity Type
     * @param <D> The DTO Type
     * @param entityInstanceCollection The Collection of Entity instances that are required to be converted to an equivalent DTO List.
     * @param DTOClass DTO Class Reference.
     * @param mappingStrategy The mapping strategy that should be utilised for the operation.
     * @return Collection<D> A Collection of the resulting DTO Instances that have been generated and had there respective values mapped
     *                 from the supplied Entity list.
     * @throws DataMappingException Where an error occurs during the mapping process.
     */
    public <E,D> Collection<D> mapToDTOCollection(Collection<? extends E> entityInstanceCollection, Class<? extends D> DTOClass, MappingStrategy... mappingStrategy) throws DataMappingException;



    /**
     * Maps the specified Collection of DTOs to an equivalent Collection of Entities of the specified type; all fields
     * with the same name are automatically mapped. Note that the returned collection type is symmetric to the collection
     * provided to the method as input. That is, if a List of Dtos is provided the a List of Entities will be returned.
     * All List and Set implementations are supported.
     * @param <D> The DTO Type
     * @param <E> The Entity Type
     * @param DTOInstanceCollection The Collection of DTO instances that are required to be converted to an equivalent list of (detached) entities.
     * @param entityClass Entity Class Reference
     * @return Collection<E> A Collection of the resulting Entity instances that have been generated and had there respective values mapped
     *                 from the supplied list of DTO's
     * @throws DataMappingException Where an error occurs during the mapping process.
     */
    public <D,E> Collection<E> mapToEntityCollection(Collection<? extends D> DTOInstanceCollection, Class<? extends E> entityClass) throws DataMappingException;
}
